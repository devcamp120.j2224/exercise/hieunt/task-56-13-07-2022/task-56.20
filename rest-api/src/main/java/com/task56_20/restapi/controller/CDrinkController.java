package com.task56_20.restapi.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56_20.restapi.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin    
	@GetMapping("/devcamp-drinks")
    public ArrayList<CDrink> getDrinkList() {
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        CDrink drink1 = new CDrink(1, "TRATAC", "Trà tắc", 10000, today, today);
        CDrink drink2 = new CDrink(2, "COCA", "Cocacola", 15000, today, today);
        CDrink drink3 = new CDrink(3, "PEPSI", "Pepsi", 15000, today, today);
        ArrayList<CDrink> drinkList = new ArrayList<>();
        drinkList.add(drink1);
        drinkList.add(drink2);
        drinkList.add(drink3);
        return drinkList;
    }
}

