package com.task56_20.restapi.model;

import java.time.LocalDate;

public class CDrink {
    private int id;
    private String maNuocUong;
    private String tenNuocUong;
    private int donGia;
    private LocalDate ngayTao;
    private LocalDate ngayCapNhat;
    // khởi tạo không tham số
    public CDrink() {
        super();
    }
    // khởi tạo đủ tất cả tham số
    public CDrink(int id, String maNuocUong,
    String tenNuocUong, int donGia,
    LocalDate ngayTao, LocalDate ngayCapNhat) {
        super();
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    // các getter và setter
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public int getDonGia() {
        return donGia;
    }
    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
    public LocalDate getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }
    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
}
